﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace RelojChecador
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnReg_Click(object sender, EventArgs e)
        {
            string empleado = txtEmpl.Text;
            string date = fecha.Text;
            string tipo;
            string texto;
            List<string> empleados = new List<string>();
            empleados.Add(empleado);
            TextWriter arch; //= new StreamWriter("registro.txt", false); // nombre, añadir o no
            if (radioEntrada.Checked)//saber si está seleccionado 
            {
                tipo = "entrada";
                texto = tipo;
                texto += "\n" + empleado + "\n" + date + "\n";


                if (!File.Exists("registro.txt"))//si  no existe el archivo
                {
                    arch = new StreamWriter("registro.txt", false);
                    arch.WriteLine(texto);
                    arch.Close();
                }
                else if ((File.Exists("registro.txt")))
                {
                    arch = new StreamWriter("registro.txt", true);
                    arch.WriteLine(texto);
                    arch.Close();
                }
            }
            else if (radioSalida.Checked)
            {
                tipo = "salida";
                texto = tipo;
                texto+= "\n" + empleado + "\n" + date + "\n";

                if (!File.Exists("registro.txt"))//si  no existe el archivo
                {
                    arch = new StreamWriter("registro.txt", false);
                    arch.WriteLine(texto);
                    arch.Close();
                }
                else if ((File.Exists("registro.txt")))
                {
                    arch = new StreamWriter("registro.txt", true);
                    arch.WriteLine(texto);
                    arch.Close();
                }
            }
            comboEmpl.Items.Add(empleado);//agregar nombre del empleado

        }

        private void btnRev_Click(object sender, EventArgs e)
        {
            string info = File.ReadAllText("registro.txt");//leer todo el archivo y cerrar
            txtRevisar.Text = info;// poner en el campo de texto la información del archivo

        }
    }
}
